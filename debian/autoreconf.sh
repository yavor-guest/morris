#!/bin/sh

set -e

intltoolize --copy --force
autoreconf --install --force
